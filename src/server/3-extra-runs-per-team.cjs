const matchFilePath = '../data/matches.csv';
const deliveryFilePath = '../data/deliveries.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/3-extra-runs-per-team.json';

function extrasPerTeam() {
    csv().fromFile(matchFilePath).then((matchObj) => {
        csv().fromFile(deliveryFilePath).then((deliveryObj) => {

            const matchesData = matchObj;
            const deliveriesData = deliveryObj;

            let idArr = {};

            for (let index = 0; index < matchesData.length; index++) {

                if (matchesData[index].season === '2016') {
                    idArr[matchesData[index].id] = 1;
                }
            }

            let teamExtras = {};

            for (let index = 0; index < deliveriesData.length; index++) {
                let delivery = deliveriesData[index];

                if (delivery.match_id in idArr) {

                    if (delivery.bowling_team in teamExtras) {
                        teamExtras[delivery.bowling_team] += Number(delivery.extra_runs);
                        
                    } else {
                        teamExtras[delivery.bowling_team] = Number(delivery.extra_runs);
                    }
                }
            }

            /* const ids = matchesData.reduce((idArr, match) => {
                if (match.season === '2016') {
                    idArr[match.id] = 1;  
                } 

                return idArr;

            }, {});

            const teamExtras = deliveriesData.reduce((dataObj, delivery) => {

                if (delivery.match_id in ids) {

                    if (delivery.bowling_team in dataObj) {
                        dataObj[delivery.bowling_team] += Number(delivery.extra_runs);
                    } else {
                        dataObj[delivery.bowling_team] = Number(delivery.extra_runs);
                    }
                }

                return dataObj;

            }, {}); */
    
            fs.writeFile(
                jsonFilePath,
                JSON.stringify(teamExtras, null, 2),
                (error) => {
                    if(error) {
                        console.error(`The file produced ${error}`);
                    } else {
                        console.log(` Successfully exported output to ${jsonFilePath}`);
                    }
            });
            
        })
    })

    .catch((error) => {
        console.error(error);
    });

}

extrasPerTeam();
