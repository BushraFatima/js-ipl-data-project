const csvFilePath = '../data/matches.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/6-highest-player-of-match-awards-per-season';

function bestPlayer() {

    csv()
    .fromFile(csvFilePath)
    .then((jsonObj)=>{
        
        const matchesData = jsonObj;

        let playerOfMatch = {};

        for (let index = 0; index < matchesData.length; index++) {

            if (matchesData[index].season in playerOfMatch) {

                if (matchesData[index].player_of_match in playerOfMatch[matchesData[index].season]) {
                    playerOfMatch[matchesData[index].season][matchesData[index].player_of_match] += 1;

                } else {
                    playerOfMatch[matchesData[index].season][matchesData[index].player_of_match] = 1;
                }

            } else {
                playerOfMatch[matchesData[index].season] = {[matchesData[index].player_of_match]: 1};
            }      
        }

        let bestPlayerOfMatch = {};

        for (year in playerOfMatch) {
            let temp = 0;
            let bestPlayer;

            for (player in playerOfMatch[year]) {

                if (playerOfMatch[year][player] > temp) {
                    temp = playerOfMatch[year][player];
                    bestPlayer = player;
                }
            }

            bestPlayerOfMatch[year] = bestPlayer;
            
        }

        fs.writeFile(
            jsonFilePath,
            JSON.stringify(bestPlayerOfMatch, null, 2),
            (error) => {
                if(error) {
                    console.error(`The file produced ${error}`);
                } else {
                    console.log(` Successfully exported output to ${jsonFilePath}`);
                }
            }
        );
        
    })

    .catch((error) => {
        console.error(error);
    });

}

bestPlayer();