const matchFilePath = '../data/matches.csv';
const deliveryFilePath = '../data/deliveries.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/4-top-10-economical-bowlers.json';

function top10Bowlers() {
    csv().fromFile(matchFilePath).then((matchObj) => {
        csv().fromFile(deliveryFilePath).then((deliveryObj) => {

            const matchesData = matchObj;
            const deliveriesData = deliveryObj;

            let ids = {};
            let bowlers = {};
            let bowlerOvers = {};

            for (let index = 0; index < matchesData.length; index++) {

                if (matchesData[index].season === '2015') {
                    ids[matchesData[index].id] = 1;
                }
            }

            for (let index = 0; index < deliveriesData.length; index++) {

                if (deliveriesData[index].match_id in ids) {

                    if (deliveriesData[index].bowler in bowlers) {
                        bowlers[deliveriesData[index].bowler] += Number(deliveriesData[index].total_runs);
                        bowlerOvers[deliveriesData[index].bowler] += 1;
    
                    } else {
                        bowlers[deliveriesData[index].bowler] = Number(deliveriesData[index].total_runs);
                        bowlerOvers[deliveriesData[index].bowler] = 1;
                    }
                }
            }
                

            for (over in bowlerOvers) {
                bowlerOvers[over] /= 6;
            }

            for (player in bowlers) {
                bowlers[player] /= bowlerOvers[player];
            }

            const sortedBowlers = Object.entries(bowlers).sort(
                (a, b) => {
                  return a[1] - b[1];
                }
              );

              const finalResult = [];

              for (let index = 0; index < 10; index++) {
                const bowlerName = sortedBowlers[index][0];
                const economy_rate = sortedBowlers[index][1];
                finalResult.push({[bowlerName]: economy_rate});
              }
    
            fs.writeFile(
                jsonFilePath,
                JSON.stringify(finalResult, null, 2),
                (error) => {

                    if(error) {
                        console.error(`The file produced ${error}`);
                    } else {
                        console.log(` Successfully exported output to ${jsonFilePath}`);
                    }
                }
            );
            
        })
    })

    .catch((error) => {
        console.error(error);
    });

}

top10Bowlers();
