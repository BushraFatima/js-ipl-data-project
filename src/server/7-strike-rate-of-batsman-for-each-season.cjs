const matchFilePath = '../data/matches.csv';
const deliveryFilePath = '../data/deliveries.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/7-strike-rate-of-batsman-for-each-season.json';

function strikeRatePerYear() {
    
    csv().fromFile(matchFilePath).then((matchObj) => {
        csv().fromFile(deliveryFilePath).then((deliveryObj) => {

            const matchesData = matchObj;
            const deliveriesData = deliveryObj;

            let batsmanStrike = {};
            let batsmanTotalBalls = {};

            for (let index1 = 0; index1 < matchesData.length; index1++) {

                for (let index2 = 0; index2 < deliveriesData.length; index2++) {

                    if (matchesData[index1].id === deliveriesData[index2].match_id) {

                        if (matchesData[index1].season in batsmanStrike) {

                            if (deliveriesData[index2].batsman in batsmanStrike[matchesData[index1].season]) {

                                batsmanStrike[matchesData[index1].season][deliveriesData[index2].batsman] +=
                                deliveriesData[index2].total_runs - deliveriesData[index2].extra_runs;

                                batsmanTotalBalls[matchesData[index1].season][deliveriesData[index2].batsman] += 1;

                            } else {

                                batsmanStrike[matchesData[index1].season][deliveriesData[index2].batsman] = 
                                deliveriesData[index2].total_runs - deliveriesData[index2].extra_runs;

                                batsmanTotalBalls[matchesData[index1].season][deliveriesData[index2].batsman] = 1;
                            }

                        } else {

                            batsmanStrike[matchesData[index1].season] = 
                            {[deliveriesData[index2].batsman]: deliveriesData[index2].total_runs - deliveriesData[index2].extra_runs};

                            batsmanTotalBalls[matchesData[index1].season] = {[deliveriesData[index2].batsman]: 1};
                        }   
                    }
                }
            }

            for (year in batsmanStrike) {

                for (player in batsmanStrike[year]) {
                    batsmanStrike[year][player] /= batsmanTotalBalls[year][player];
                    batsmanStrike[year][player] *= 100;

                }
            }

            fs.writeFile(
                jsonFilePath,
                JSON.stringify(batsmanStrike, null, 2),
                (error) => {
                    if(error) {
                        console.error(`The file produced ${error}`);
                    } else {
                        console.log(` Successfully exported output to ${jsonFilePath}`);
                    }
            });
            
        })
    })

    .catch((error) => {
        console.error(error);
    });

}

strikeRatePerYear();
