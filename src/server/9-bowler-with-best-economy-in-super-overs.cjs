const csvFilePath = '../data/deliveries.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/9-bowler-with-best-economy-in-super-overs.json';

function getBestEconomyBowler() {

    csv()
    .fromFile(csvFilePath)
    .then((jsonObj)=>{
        
        const deliveriesData = jsonObj;

        let bowlersEconomyRate = {};
        let bowlerOvers = {};

        for (let index = 0; index < deliveriesData.length; index++) {

            if (deliveriesData[index].is_super_over != '0') {
                if (deliveriesData[index].bowler in bowlersEconomyRate) {
                    bowlersEconomyRate[deliveriesData[index].bowler] += Number(deliveriesData[index].total_runs);
                    bowlerOvers[deliveriesData[index].bowler] += 1;
                    
                } else {
                    bowlersEconomyRate[deliveriesData[index].bowler] = Number(deliveriesData[index].total_runs);
                    bowlerOvers[deliveriesData[index].bowler] = 1;
                }    
            }   
        }

        for (over in bowlerOvers) {
            bowlerOvers[over] /= 6;
        }

        for (player in bowlersEconomyRate) {
            bowlersEconomyRate[player] /= bowlerOvers[player];
        }

        const sortedBowlersEconomyRate = Object.entries(bowlersEconomyRate).sort(
            (a, b) => {
              return a[1] - b[1];
            }
        );

        const finalResult = [];

        for (let index = 0; index < sortedBowlersEconomyRate.length; index++) {
        const bowlerName = sortedBowlersEconomyRate[index][0];
        const economy_rate = sortedBowlersEconomyRate[index][1];
        finalResult.push({[bowlerName]: economy_rate});
        }

        fs.writeFile(
            jsonFilePath,
            JSON.stringify(finalResult[0], null, 2),
            (error) => {
                if(error) {
                    console.error(`The file produced ${error}`);
                } else {
                    console.log(` Successfully exported output to ${jsonFilePath}`);
                }
            }
        );
        
    })

    .catch((error) => {
        console.error(error);
    });

}

getBestEconomyBowler();
