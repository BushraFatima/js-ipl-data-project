const csvFilePath = '../data/matches.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/1-matches-per-year.json';

function countMatches() {

    csv()
    .fromFile(csvFilePath)
    .then((jsonObj)=>{
        
        const matchesData = jsonObj;

        const matchPerYear = {};

        for (let index = 0; index < matchesData.length; index++) {
            let year = matchesData[index].season;

            if (matchesData[index].season in matchPerYear) {
                matchPerYear[year] += 1;
                
            } else {
                matchPerYear[year] = 1;
            }
        }

        /* const matchPerYear = matchesData.reduce((matchObj, match) => {

            if (match.season in matchObj) {
                matchObj[match.season] += 1;
                return matchObj;

            } else {
                matchObj[match.season] = 1;
                return matchObj;  
            }
            
        }, {}); */

        fs.writeFile(
            jsonFilePath,
            JSON.stringify(matchPerYear, null, 2),
            (error) => {
                if(error) {
                    console.error(`The file produced ${error}`);
                } else {
                    console.log(` Successfully exported output to ${jsonFilePath}`);
                }
            }
        );
        
    })

    .catch((error) => {
        console.error(error);
    });

}

countMatches();



