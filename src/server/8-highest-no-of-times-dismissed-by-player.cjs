const csvFilePath = '../data/deliveries.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/8-highest-no-of-times-dismissed-by-player.json';

function getpPlayerDismissedMax() {

    csv()
    .fromFile(csvFilePath)
    .then((jsonObj)=>{
        
        const deliveriesData = jsonObj;

        let playerDismissCount = {};

        for (let index = 0; index < deliveriesData.length; index++) {

            if (deliveriesData[index].player_dismissed) {

                if (deliveriesData[index].player_dismissed in playerDismissCount) {
                    playerDismissCount[deliveriesData[index].player_dismissed] += 1;

                } else {
                    playerDismissCount[deliveriesData[index].player_dismissed] = 1;
                }
            }     
        }

        let dismissCount = Object.values(playerDismissCount);
        dismissCount.sort((a, b) => { return a - b; });
        let maxDismissCount = dismissCount[dismissCount.length - 1];
        let playerDismissedMax;

        for (player in playerDismissCount) {
            if (playerDismissCount[player] === maxDismissCount) {
                playerDismissedMax = {[player]: maxDismissCount};
            }
        }
        
        fs.writeFile(
            jsonFilePath,
            JSON.stringify(playerDismissedMax, null, 2),
            (error) => {
                if(error) {
                    console.error(`The file produced ${error}`);
                } else {
                    console.log(` Successfully exported output to ${jsonFilePath}`);
                }
            }
        );
        
    })

    .catch((error) => {
        console.error(error);
    });

}

getpPlayerDismissedMax();
