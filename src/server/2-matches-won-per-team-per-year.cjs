const csvFilePath = '../data/matches.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/2-matches-won-per-team-per-year.json';

function matchesTeamYr() {

    csv()
    .fromFile(csvFilePath)
    .then((jsonObj)=>{
        
        const matchesData = jsonObj;

        let matchPerTeamYr = {};

        for (let index = 0; index < matchesData.length; index++) {
            let match = matchesData[index];
            
            if (match.season in matchPerTeamYr) {

                if (match.winner in matchPerTeamYr[match.season]) {
                    matchPerTeamYr[match.season][match.winner] += 1;

                } else {
                    matchPerTeamYr[match.season][match.winner] = 1;
                }

            } else {
                matchPerTeamYr[match.season] = {[match.winner]: 1};
            }
        }

        /* const matchPerTeamYr = matchesData.reduce((matchObj, match) => {

            if (match.season in matchObj) {

                if (match.winner in matchObj[match.season]) {
                    matchObj[match.season][match.winner] += 1;
                } else {
                    matchObj[match.season][match.winner] = 1;
                }

            } else {
                matchObj[match.season] = {[match.winner]: 1};
            }
            
            return matchObj;

        }, {});   */

        fs.writeFile(
            jsonFilePath,
            JSON.stringify(matchPerTeamYr, null, 2),
            (error) => {
                if(error) {
                    console.error(`The file produced ${error}`);
                } else {
                    console.log(` Successfully exported output to ${jsonFilePath}`);
                }
            });
        
    })

    .catch((error) => {
        console.error(error);
    });

}

matchesTeamYr();


