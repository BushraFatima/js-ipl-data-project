const csvFilePath = '../data/matches.csv';
const csv = require('csvtojson');
const fs = require('fs');
const jsonFilePath = '../public/output/5-each-team-won-toss-won-match.json';

function teamsWonTossMatch() {

    csv()
    .fromFile(csvFilePath)
    .then((jsonObj)=>{
        
        const matchesData = jsonObj;

        let teamWon = {};

        for (let index = 0; index < matchesData.length; index++) {

            if (matchesData[index].toss_winner === matchesData[index].winner) {

                if (matchesData[index].toss_winner in teamWon) {
                    teamWon[matchesData[index].toss_winner] += 1;

                } else {
                teamWon[matchesData[index].toss_winner] = 1;
                }
            }        
        }

        fs.writeFile(
            jsonFilePath,
            JSON.stringify(teamWon, null, 2),
            (error) => {
                if(error) {
                    console.error(`The file produced ${error}`);
                } else {
                    console.log(` Successfully exported output to ${jsonFilePath}`);
                }
            }
        );
        
    })

    .catch((error) => {
        console.error(error);
    });

}

teamsWonTossMatch();
